<?php
namespace App;

class App
{
    protected $app;

    public function __construct($container)
    {
        $this->app = $container;
    }
}
