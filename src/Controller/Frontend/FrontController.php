<?php

namespace App\Controller\Frontend;

use App\App;
use Slim\Http\Response;
use Slim\Http\Request;

class FrontController extends App
{
    public function index(Request $request, Response $response, array $args)
    {
        // Sample log message
        $this->app->logger->info("Slim-Skeleton '/' route");
        $names = [
            'uno',
            'dos',
        ];
        // Render index view
        return $this->app->view->render($response, 'index.html.twig', $args);
    }
}
