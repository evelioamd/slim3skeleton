<?php
use App\Controller\Frontend\FrontController;

$container['FrontEnd\FrontController'] = function ($container) {
    return new FrontController($container);
};
