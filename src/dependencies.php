<?php

use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Loader\PhpFileLoader;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

// DIC configuration
$container = $app->getContainer();

//translator service
// First param is the "default language" to use.
$translator = new Translator('es_MX', new MessageSelector());
// Set a fallback language incase you don't have a translation in the default language
$translator->setFallbackLocales(['es_MX']);
// Add a loader that will get the php files we are going to store our translations in
$translator->addLoader('php', new PhpFileLoader());
// Add language files here
$translator->addResource('php', __DIR__ . '/../translations/en_US.php', 'en_US'); // English
$translator->addResource('php', __DIR__ . '/../translations/es_MX.php', 'es_MX'); // Spanish

$container['translator'] = function ($container) use ($translator) {
    return $translator;
};
// view
$container['view'] = function ($container) use ($translator) {
    $view = new Twig(__DIR__ . '/../templates', [
        'cache' => __DIR__ . '/../var/cache',
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new TwigExtension($container['router'], $basePath));
    $view->addExtension(new TranslationExtension($translator));

    return $view;
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};
